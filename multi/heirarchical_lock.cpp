#include <mutex>

class heirarchical_mutex{
    private:
        std::mutex internal_mutex;
        unsigned long const heirarchy_val;
        unsigned long previous_heirarchy_val;
        static thread_local unsigned this_thread_heirarchy_val;

        void check_for_heirarchy_violation(){
            if (this_thread_heirarchy_val<=heirarchy_val){
                throw std::logic_error("Mutex Heirarchy Violated.");
            }
        }

        void update_heirarchy_val(){
            previous_heirarchy_val=this_thread_heirarchy_val;
            this_thread_heirarchy_val=heirarchy_val;
        }

    public:
        explicit heirarchical_mutex(unsigned long val):
            heirarchical_val(val),
            previous_heirarchy_val(0)
    {}

        void lock(){
            check_for_heirarchy_violation();
            interal_mutex.lock();
            update_heirarchy_val();
        }

        void unlock(){
            if (this_thread_heirarchy_val!=heirarchy_val){
                throw std::logic_error("Mutex Heirarchy Violated.");
            }
            this_thread_heirarchy_val=previous_heirarchy_val;
            internal_mutex.unlock();
        }



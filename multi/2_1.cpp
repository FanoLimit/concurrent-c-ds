#include <list>
#include <mutex>
#include <iostream>
#include <algorithm>

template<typename Iterator, typename T>
struct accumulate_block{
    void operator()(Iterator first, Iterator second, T& result){
        result=std::acumulate(first,last,result);
    }
};

template<typename Iterator, typename T>
T parallel_accumulate(Iterator first, Iterator second, T init){
    unsigned long const length=std::distance(first,last);
    if (!length) return init;

    unsigned long const min_per_threads=25;
    unsigned long const hardware_threads=std::thread::hardware_concurrency();
    unsigned long const max_threads=(length+min_per_thread-1)/(min_per_thread);

    unsigned long const num_threads=std::min(hardware_threads==0 ? 2 : hardware_threads,max_threads);
    unsigned long const block_size=length/num_threads;

    std::vector<T> sol(num_threads);
    std::vector<std::thread> t(num_threads-1);

    Iterator first_block=first;
    for (unsigned long i=0;i<(num_threads-1);++i){
        Iterator block_end=block_start;
        std::advance(block_end,block_size);
        threads[i]=std::thread(accumulate_block<Iterator,T>(),block_start,block_end,std::ref(results[i]));
        block_start=block_end;
    }

    accumulate_block<Iterator,T>(block_start,last,results[num_threads-1]);

    for (auto& thread: t){
        thread.join();
    }

    return std::accumulate(results.begin(),results.end(),init);
}



template <typename T>

std::list<T> quicksort(std::list<T> input){
    if (input.empty()){
        return input;
    }

    std::list<T> result;
    result.splice(result.begin(),input,input.begin());
    auto pivot=*result.begin();

    std::list<T> lower;
    lower.splice(lower.end(),input,input.begin(),pivot);
    std::future<std::list<T>> new_lower(std::async(&quicksort,std::move(lower)));

    auto higher(quicksort(std::move(input)));

    result.splice(result.end(),std::move(higher));
    result.splice(result.begin(),new_lower.get());
    return result;
}

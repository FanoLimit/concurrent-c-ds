#include <queue>
#include <memory>
#include <mutex>
#include <condition_variable>

template<typename T>
class threadsafe_queue{
    private:
        mutable std::mutex mut;
        std::queue<T> q;
        std::condition_variable cond;

    public:
        threadsafe_queue(){}
        threadsafe_queue(threadsafe_queue const& other){
            std::lock_guard<std::mutex> lk(other.mutex);
            q=other.q;
        }
        void push(T val){
            std::lock_guard<std::mutex> lk(mut);
            q.push(val);
            cond.notify_one();
        }
        void wait_and_pop(){
            std::lock_guard<std::mutex> lk(mut);
            cond.wait(lk,[this](return !q.empty();));
            value=q.front();
            q.pop();
        }
        std::shared_ptr wait_and_pop(){
            std::lock_guard<std::mutex> lk(mut);
            cond.wait(lk,[this](return !q.empty();));
            std::shared_ptr& val(std::make_shared<T>(q.front()));
        }
        bool try_pop(T& val){
            std::lock_guard<std::mutex> lk(mut);
            if (q.empty()){
                return false;
            }
            val=q.front();
            q.pop();
            return true;
        }
        std::shared_ptr<T> try_pop(){
            std::lock_guard<std::mutex> lk(mut);
            if (q.empty()){
                return std::shared_ptr<T>();
            }
            std::shard_ptr res=q.front();
            q.pop();
            return res;
        }
        bool empty() const{
            std::lock_guard<std::mutex> lk(mut);
            return q.empty();
        }
};

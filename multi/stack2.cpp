#include <memory>
#include <mutex>
#include <exception>
#include <stack>

struct empty_stack : std::exception{
    const char* what() const throw();
};

template<typename T>
class shareable_stack{
    private:
        std::stack<T> data;
        mutable std::mutex m;

    public:
        shareable_stack(){};
        shareable_stack(const shareable_stack& other){
            std::lock_guard<std::mutex> lock(m);
            data=other.data;
        }
        shareable_stack operator=(shareable_stack& other)=delete;
        void push(T val){
            std::lock_guard<std::mutex> lock(m);
            data.push(std::move(value));
        }

        std::shared_ptr<T> pop(){
            std::lock_guard<std::mutex> lock(m);
            if (data.empty()){
                throw empty_stack();
            }

            std::shared_ptr res=std::make_shared<T> (data.top());
            data.pop();
            return res;
        }

        void pop(T& res){
            std::lock_guard<std::mutex> lock(m);
            if (data.empty()){
                throw empty_stack();
            }

            res=data.top();
            data.pop();
        }

        bool empty(){
            std::lock_guard<std::mutex> lock(m);
            return data.empty();
        }
};

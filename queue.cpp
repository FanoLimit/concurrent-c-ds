#include <mutex>
#include <queue>
#include <condition_variable>

//Note that if an exception is thrown in wait_and_pop, the entire class fails

template<typename T>
class threadsafe_queue{
    private:
        std::queue<T> data;
        mutable std::mutex m;
        std::condition_variable cv;
    public:
        threadsafe_queue(){}

        void push(T& val){
            std::lock_guard<std::mutex> lk(m);
            data.push(std::move(val));
            cv.notify_one();
        }

        void wait_and_pop(T& res){
            std::lock_guard<std::mutex> lk(m);
            cv.wait(lk,[this]{return !data.empty();});
            res=std::move(data.front());
            data.pop();
        }

        std::shared_ptr<T> wait_and_pop(){
            std::unique_lock<std::mutex> lk(m);
            cv.wait(lk,[this]{return !data.empty();});
            std::shared_ptr<T> res(std::make_shared(std::move(data.front())));
            data.pop();
            return res;
        }

        bool try_pop(T& res){
            std::lock_guard<std::mutex> lk(m);
            if (data.empty()){
                return false;
            }
            res=std::move(data.front());
            data.pop();
            return true;
        }

        std::shared_ptr<T> try_pop(){
            std::lock_guard<std::mutex> lk(m);
            if (data.empty()){
                return std::shared_ptr<T> ();
            }
            std::shared_ptr res(std::make_shared<T> (std::move(data.front())));
            return res;
        }

        bool empty() const{
            std::lock_guard<std::mutex> lk(m);
            return data.empty();
        }
};

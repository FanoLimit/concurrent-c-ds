#include <mutex>
#include <queue>
#include <condition_variable>

template<typename T>
class threadsafe_queue{
    private:
        std::queue<std::share_ptr<T > data;
        mutable std::mutex m;
        std::condition_variable cv;

    public:
        threadsafe_queue(){}

        void wait_and_pop (T& res){
            std::unique_lock<std::mutex> lk(m);
            cv.wait(lk,[this]{return !data.empty();});
            res=std::move(*data.front());
            data.pop();
        }

        bool try_pop(T& res){
            std::lock_guard<std::mutex> lk(m);
            if (data.empty()){
                return false;
            }
            res=std::move(*data.front());
            data.pop();
            return true;
        }

        std::shared_ptr<T> wait_and_pop(){
            std::unique_lock<std::mutex> lk(m);
            cv.wait(lk,[this]{return !data.empty();});
            std::shared_ptr<T> res=data.front();
            data.pop();
            return res;
        }

        std::shared_ptr<T> try_pop(){
            std::lock_guard<std::mutex> lk(m);
            if (data.empty()){
                return std::shared_ptr<T>();
            }
            std::shared_ptr<T> res=data.front();
            data.pop();
            return res;
        }

        void push(T val){
            std::shared_ptr new_val(std::make_shared<T> (std::move(val)));
            std::lock_guard<std::mutex> lk(m);
            data.push(new_val);
            cv.notify_one();
        }

        bool empty() const{
            std::lock_guard<std::mutex> lk(m);
            return data.empty();
        }
};


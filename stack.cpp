#include <exception>
#include <stack>
#include <mutex>

struct empty_stack: std::exception{
    const char* what() const throw();
};

template<typename T>
class threadsafe_stack{
    private:
        std::stack<T> data;
        mutable std::mutex<T> m;
    public:
        threadsafe_stack(){}

        threadsafe_stack(const threadsafe_stack& other){
            std::lock_guard<std::mutex> lk(m);
            data=other.data;
        }

        threadsafe_stack operator=(const threadsafe_stack&) = delete;

        void push(T& val){
            std::lock_guard<std::mutex> lk(m);
            data.push(std::move(val));
        }

        std::shared_ptr<T> pop(){
            std::lock_guard<std::mutex> lk(m);
            if (data.empty()){
                throw empty_stack();
            }
            std::shared_ptr<T> res(std::make_shared<T> (std::move(data.top())));
            data.pop();
            return res;
        }

        void pop(T& res){
            std::lock_guard<std::mutex> lk(m);
            if (data.empty()){
                throw empty_stack();
            }
            res=std::move(data.top());
            data.pop();
        }
        
        bool empty() const{
            std::lock_guard<std::mutex> lk(m);
            return data.empty();
        }
};

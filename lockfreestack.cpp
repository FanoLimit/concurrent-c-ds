template<typename T>
class lock_free_stack{
    private:
        struct node{
            std::shared_ptr<T> data;
            node* nxt;
            node(T const& other):
                data(std::make_shared<T> (other)){}
            };
        std::atomic<node*> head;

    public:
        void push(T const& data){
            node* const new_node=std::make_shared<T> data;
            new_node->nxt=head.load();
            while (!head.compare_and_exchange(new_node->nxt,new_node));
        }

        std::shared_ptr<T> pop(){
            node* old_head=head.load();
            while (old_head && !head.compare_and_exchange(old_head,old_head->nxt));
            return old_head ? old_head->data : std::shared_ptr<T> ();
        }
};

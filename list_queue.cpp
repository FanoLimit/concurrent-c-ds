#include <mutex>
#include <condition_variable>

template<typename T>
class queue{
    private:
        struct node{
            std::shared_ptr<T> data;
            std::unique_ptr<node> nxt;
        };
        std::unique_ptr<node> head;
        node* tail;
        std::mutex head_mutex;
        std::mutex tail_mutex;
        node* get_tail(){
            std::lock_guard<std::mutex> tail_lock(tail_mutex);
            return tail;
        }
        std::unique_ptr<node> pop_head(){
            std::lock_guard<std::mutex> head_lock(head_mutex);
            if (head.get()==get_tail()){
                return nullptr;
            }
            std::unique_ptr<node> old_head = std::move(head);
            head=std::move(old_head->next);
            return old_head;
        }
    public:
        threadsafe_queue():head(new node),tail(head.get())
    {}
        threadsafe_queue(const threadsafe& other)=delete;
        threadsafe_queue operator=(const threadsafe& other)=delete;

        std::shared_ptr<T> try_pop(){
            std::unique_ptr<node> old_head=pop_head();
            return old_head ? old_head->data : std::shared_ptr<T> ();
        }

        void push(T val){
            std::shared_ptr<T> new_data(std::make_shared(std::move(T)));
            std::unique_ptr<node> p(new node);
            node* const new_tail=p.get();
            std::lock_guard<std::mutex> tail_lock(tail_mutex);
            tail->data=new_data;
            tail->nxt=std::move(p);
            tail=new_tail;
        }
};

              
